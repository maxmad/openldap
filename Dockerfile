FROM alpine:3.18

RUN apk --no-cache add openldap-back-mdb openldap && \
    mkdir -p /run/openldap /var/lib/openldap/openldap-data && \
    rm -rf /var/cache/apk/*

COPY config/* /etc/openldap/

COPY docker-entrypoint.sh /

EXPOSE 389

EXPOSE 636

VOLUME ["/ldif", "/var/lib/openldap/openldap-data"]

ENTRYPOINT ["/docker-entrypoint.sh"]
