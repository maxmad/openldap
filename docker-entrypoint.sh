#!/bin/sh
# docker entrypoint script
# configures and starts LDAP

LDAPS=false

# replace variables in slapd.conf
SLAPD_CONF="/etc/openldap/slapd.conf"

# comment out TLS configuration
sed -i "s~TLSCACertificateFile~#&~" "$SLAPD_CONF"
sed -i "s~TLSCertificateKeyFile~#&~" "$SLAPD_CONF"
sed -i "s~TLSCertificateFile~#&~" "$SLAPD_CONF"
sed -i "s~TLSVerifyClient~#&~" "$SLAPD_CONF"

sed -i "s~%ROOT_USER%~$ROOT_USER~g" "$SLAPD_CONF"
sed -i "s~%SUFFIX%~$SUFFIX~g" "$SLAPD_CONF"
sed -i "s~%ACCESS_CONTROL%~$ACCESS_CONTROL~g" "$SLAPD_CONF"

# encrypt root password before replacing
ROOT_PW=$(slappasswd -s "$ROOT_PW")
sed -i "s~%ROOT_PW%~$ROOT_PW~g" "$SLAPD_CONF"

# replace variables in organisation configuration
ORG_CONF="/etc/openldap/organisation.ldif"
sed -i "s~%SUFFIX%~$SUFFIX~g" "$ORG_CONF"
sed -i "s~%ORGANISATION_NAME%~$ORGANISATION_NAME~g" "$ORG_CONF"

# add organisation and users to ldap (order is important)
slapadd -l "$ORG_CONF"

# add any scripts in ldif
for l in /ldif/*; do
  case "$l" in
    *.ldif)  echo "ENTRYPOINT: adding $l";
            slapadd -l $l
            ;;
    *)      echo "ENTRYPOINT: ignoring $l" ;;
  esac
done

echo "Starting LDAP"
slapd -d "$LOG_LEVEL" -h "ldap:///"

# run command passed to docker run
exec "$@"
