# usage

```sh
cat << EOF > .env
# OPENLDAP
LDAP_HOST=openldap
ROOT_USER=admin
ROOT_PW=RCbB5L4J7TnlvQXPIpEOfQhYJrZS4ckI72jC4rAIP7U
ORGANISATION_NAME="Example Ltd"
SUFFIX=dc=example,dc=com

ACCESS_CONTROL=access to * by * read
LOG_LEVEL=stats
EOF
```
